﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class UserBook
    {
        /// <summary>
        /// Form unique ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// User that filled out the form name
        /// </summary>
        [Required(ErrorMessage = "Enter your name")]
        [StringLength(10, MinimumLength = 3)]
        public string UserName { get; set; }
        /// <summary>
        /// user story of book love
        /// </summary>
        [Required(ErrorMessage = "Enter your city")]
        [StringLength(10, MinimumLength = 3)]
        public string City { get; set; }
        /// <summary>
        /// user favourite book
        /// </summary>
        [Required(ErrorMessage = "Choose at least one option")]
        public string RadioAns { get; set; }
        /// <summary>
        /// Volumes user might know
        /// </summary>
        public virtual List<Book> Books { get; set; }

        /// <summary>
        /// FormModel constructor with params
        /// </summary>
        /// <param name="name">user name</param>
        /// <param name="story">user love books story</param>
        /// <param name="favbook">favourite book</param>
        /// <param name="Volumes">list of known or not series</param>
        public UserBook(string name, string city, string favbook, List<Book> books)
        {
            UserName = name;
            City = city;
            RadioAns = favbook;
            Books = books;
        }
        /// <summary>
        /// FormModel paramless constructor
        /// </summary>
        public UserBook()
        {
            Books = new List<Book>();
        }
    }
}