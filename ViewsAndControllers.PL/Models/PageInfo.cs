﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class PageInfo
    {
        public int PageNumber { get; set; } // номер поточної сторінки
        public int PageSize { get; set; } // кількість об'єктів на сторінці
        public int TotalItems { get; set; } // всього об'єктів
        public int TotalPages  // всього сторінок
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
}