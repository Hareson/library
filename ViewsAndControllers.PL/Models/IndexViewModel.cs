﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class IndexViewModel
    {
        public string UserName { get; set; }
        public string Text { get; set; }        


        public IEnumerable<Article> Articles { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}