﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class Book
    {
        /// <summary>
        /// Volume unique ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Volume Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// If user knows or not 
        /// </summary>
        public bool CheckAns { get; set; }

        /// <summary>
        /// Volume constructor with params
        /// </summary>
        /// <param name="name">volume name</param>
        /// <param name="know">if user knows or not</param>
        public Book(string name, bool know)
        {            
            Name = name;
            CheckAns = know;
        }

        public Book()
        { }
    }
}