﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime DateOfPublishing { get; set; }

        public Comment() { }
        public Comment(string userName, string text, DateTime dateOfPublishing)
        {
            UserName = userName;
            Text = text;
            DateOfPublishing = dateOfPublishing;
        }
    }
}