﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsAndControllers.PL.Models
{
    public class Article
    {
        /// <summary>
        /// article unique ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Article heading
        /// </summary>
        public string Heading { get; set; }
        /// <summary>
        /// Article main body text
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// DAte of publishing this article
        /// </summary>
        public DateTime DateOfPublishing { get; set; }

        /// <summary>
        /// Article Construstor with params
        /// </summary>
        /// <param name="heading">Article heading</param>
        /// <param name="text">article main text</param>
        /// <param name="date">date of publishing</param>
        public Article(string heading, string text, DateTime date)
        {
            Heading = heading;
            Text = text;
            DateOfPublishing = date;
        }

        public Article()
        { }
    }
}