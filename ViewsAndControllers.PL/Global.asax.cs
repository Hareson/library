using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ViewsAndControllers.BLL.Infrastructure;
using ViewsAndControllers.PL.Util;

namespace ViewsAndControllers.PL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule serviceModule = new ServiceModule("DefaultConnection");            
            NinjectModule userBookModule = new UserBookModule();
            NinjectModule bookModule = new BookModule();
            NinjectModule articleModule = new ArticleModule();
            NinjectModule commentModule = new CommentModule();
            var kernel = new StandardKernel(serviceModule, userBookModule, bookModule, articleModule, commentModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
