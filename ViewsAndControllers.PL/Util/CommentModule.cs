﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.BLL.Services;

namespace ViewsAndControllers.PL.Util
{
    public class CommentModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICommentService>().To<CommentService>();
        }
    }
}