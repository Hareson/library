﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.BLL.Services;

namespace ViewsAndControllers.PL.Util
{
    public class UserBookModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserBookService>().To<UserBookService>();
        }
    }    
    
}