﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.PL.Models;

namespace ViewsAndControllers.PL.Controllers
{
    public class GuestController : Controller
    {
        private ICommentService commentService;

        public GuestController(ICommentService service)
        {
            commentService = service;
        }


        public ActionResult Guest(int page = 1)
        {
            var commentDTOs = commentService.GetComments();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CommentDTO, Comment>()).CreateMapper();
            var comments = mapper.Map<IEnumerable<CommentDTO>, List<Comment>>(commentDTOs);

            int pageSize = 3; // кількість об'єктів на сторінку
            IEnumerable<Comment> commentsPerPages = comments.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comments.Count };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Comments = commentsPerPages };

            return View(ivm);
        }

        [HttpPost]
        public ActionResult Guest(IndexViewModel model)
        {
            CommentDTO commentDTO = new CommentDTO
            {
                UserName = model.UserName,
                Text = model.Text,
                DateOfPublishing = DateTime.Now
            };

            commentService.AddComment(commentDTO);
            

            return RedirectToAction("Guest");
        }
    }
}