﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.PL.Models;

namespace Views_and_Controllers.Controllers
{
    public class HomeController : Controller
    {
        private IArticleService articleService;

        public HomeController(IArticleService service)
        {
            articleService = service;
        }

        public ActionResult Index(int page = 1)
        {
            var articleDTOs = articleService.GetArticles();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDTO, Article>()).CreateMapper();
            var articles = mapper.Map<IEnumerable<ArticleDTO>, List<Article>>(articleDTOs);            

            int pageSize = 3; // кількість об'єктів на сторінку
            IEnumerable<Article> articlesPerPages = articles.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = articles.Count };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Articles = articlesPerPages };

            return View(ivm);
        }        
        
        public ActionResult Article(Article article)
        {
            return View(article);
        }

    }
}