﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.PL.Models;

namespace ViewsAndControllers.PL.Controllers
{
    public class QuestionnaireController : Controller
    {
        private IUserBookService userBookService;
        private IBookService bookService;

        public QuestionnaireController(IUserBookService service, IBookService bookServ)
        {
            userBookService = service;
            bookService = bookServ;
        }        

        public ActionResult Questionnaire()
        {
            UserBook userBook = new UserBook();
            ViewBag.favbook = "1";
            var bookDTOs = bookService.GetBooks();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<BookDTO, Book>()).CreateMapper();
            var books = mapper.Map<IEnumerable<BookDTO>, List<Book>>(bookDTOs);
            userBook.Books = books;
            return View(userBook);
        }

        [HttpPost]
        public ActionResult Questionnaire(UserBook ui)
        {
            if (String.IsNullOrEmpty(ui.UserName) || String.IsNullOrEmpty(ui.City) ||
                String.IsNullOrEmpty(ui.RadioAns))
            {
                if (ui.Books.All(c => c.CheckAns == false))
                {
                    ModelState.AddModelError("CheckAns", "Choose your read books");
                }
                return View(ui);
            }

            if (ui.Books.All(c => c.CheckAns == false))
            {
                ModelState.AddModelError("CheckAns", "Choose your read books");
                return View(ui);
            }


            StringBuilder sb = new StringBuilder();
            foreach (var i in ui.Books)
            {
                if (i.CheckAns)
                {
                    sb.Append(i.Name + ", ");
                }
            }

            ViewBag.SelectBook = sb.ToString().Remove(sb.Length - 2);


            return View("UserInfo", ui);
        }
    }
}