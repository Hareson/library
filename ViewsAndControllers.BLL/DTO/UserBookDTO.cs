﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewsAndControllers.BLL.DTO
{
    public class UserBookDTO
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string City { get; set; }
        public string RadioAns { get; set; }
        public virtual List<BookDTO> books { get; set; }
    }
}
