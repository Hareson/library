﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewsAndControllers.BLL.DTO
{
    public class BookDTO
    {       
        public int ID { get; set; }
        public string Name { get; set; }
        public bool CheckAns { get; set; }
        
    }
}
