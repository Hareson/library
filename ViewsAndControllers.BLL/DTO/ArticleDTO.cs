﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewsAndControllers.BLL.DTO
{
    public class ArticleDTO
    {
        public int ID { get; set; }
        public string Heading { get; set; }
        public string Text { get; set; }
        public DateTime? DateOfPublishing { get; set; }
    }
}
