﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Infrastructure;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.Interfaces;

namespace ViewsAndControllers.BLL.Services
{
    public class UserBookService : IUserBookService
    {
        IUnitOfWork unitOfWork { get; set; }

        public UserBookService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public void AddUserBook(UserBookDTO userBookDTO)
        {
            List<Book> books = new List<Book>();
            foreach (BookDTO book in userBookDTO.books)
            {
                books.Add(new Book
                {
                    Name = book.Name,
                    CheckAns = book.CheckAns
                });

            }
            UserBook userBook = new UserBook
            {
                UserName = userBookDTO.UserName,
                City = userBookDTO.City,
                RadioAns = userBookDTO.RadioAns,
                books = books
            };
            unitOfWork.UserBookRepo.Add(userBook);
            unitOfWork.Complete();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        public UserBookDTO GetUserBook(int? id)
        {
            if (id == null)
                throw new ValidationException("ID couldn't be found", "");
            UserBook userBook = unitOfWork.UserBookRepo.Get(id.Value);
            if (userBook == null)
                throw new ValidationException("Form wasn't found", "");
            List<BookDTO> books = new List<BookDTO>();
            foreach (Book book in userBook.books)
            {
                books.Add(new BookDTO
                {
                    ID = book.ID,
                    Name = book.Name,
                    CheckAns = book.CheckAns
                });

            }
            return new UserBookDTO
            {
                UserName = userBook.UserName,
                City = userBook.City,
                RadioAns = userBook.RadioAns,
                books = books
            };
        }

        public IEnumerable<UserBookDTO> GetUserBooks()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserBook, UserBookDTO>()
            .ForMember(d => d.books, o => o.MapFrom(s => s.books.Select(c => c.ID).ToArray()))).CreateMapper();
            return mapper.Map<IEnumerable<UserBook>, List<UserBookDTO>>(unitOfWork.UserBookRepo.GetAll());
        }
    }
}
