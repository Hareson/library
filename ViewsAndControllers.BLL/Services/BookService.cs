﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Infrastructure;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.UnitOfWork;

namespace ViewsAndControllers.BLL.Services
{
    public class BookService : IBookService
    {
        public UnitOfWork unitOfWork { get; set; }
        public BookService(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public void AddBook(BookDTO bookDTO)
        {
            Book book = new Book
            {
                Name = bookDTO.Name,
                CheckAns = bookDTO.CheckAns
            };
            unitOfWork.BookRepo.Add(book);
            unitOfWork.Complete();
        }
        public void Dispose()
        {
            unitOfWork.Dispose();
        }
        public BookDTO GetBook(int? id)
        {
            if (id == null)
                throw new ValidationException("ID couldn't be found", "");
            Book book = unitOfWork.BookRepo.Get(id.Value);
            if (book == null)
                throw new ValidationException("Volume wasn't found", "");
            return new BookDTO
            {
                Name = book.Name,
                CheckAns = book.CheckAns,
                ID = book.ID
            };

        }
        public IEnumerable<BookDTO> GetBooks()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Book, BookDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Book>, List<BookDTO>>(unitOfWork.BookRepo.GetAll());
        }
    }
}
