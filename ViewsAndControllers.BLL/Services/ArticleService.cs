﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Infrastructure;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.Interfaces;
using ViewsAndControllers.DAL.UnitOfWork;

namespace ViewsAndControllers.BLL.Services
{
    public class ArticleService : IArticleService
    {
        IUnitOfWork unitOfWork { get; set; }

        public ArticleService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public void AddArticle(ArticleDTO articleDTO)
        {
            Article article = new Article
            {
                Heading = articleDTO.Heading,
                Text = articleDTO.Text,
                DateOfPublishing = articleDTO.DateOfPublishing.Value
            };
            unitOfWork.ArticleRepo.Add(article);
            unitOfWork.Complete();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        public ArticleDTO GetArticle(int? id)
        {
            if (id == null)
                throw new ValidationException("Id can't be found", "");

            Article article = unitOfWork.ArticleRepo.Get(id.Value);
            if (article == null)
                throw new ValidationException("Article not found", "");
            return new ArticleDTO
            {
                ID = article.ID,
                Heading = article.Heading,
                Text = article.Text,
                DateOfPublishing = article.DateOfPublishing
            };
        }

        public IEnumerable<ArticleDTO> GetArticles()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Article, ArticleDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Article>, List<ArticleDTO>>(unitOfWork.ArticleRepo.GetAll());
        }
    }
}
