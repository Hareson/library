﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;
using ViewsAndControllers.BLL.Infrastructure;
using ViewsAndControllers.BLL.Interfaces;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.Interfaces;

namespace ViewsAndControllers.BLL.Services
{
    public class CommentService : ICommentService
    {
        IUnitOfWork unitOfWork { get; set; }

        public CommentService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public void AddComment(CommentDTO commentDTO)
        {
            Comment comment = new Comment
            {
                UserName = commentDTO.UserName,
                Text = commentDTO.Text,
                DateOfPublishing = commentDTO.DateOfPublishing.Value
            };
            unitOfWork.CommentRepo.Add(comment);
            unitOfWork.Complete();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        public CommentDTO GetComment(int? id)
        {
            if (id == null)
                throw new ValidationException("Id can't be found", "");

            Comment comment = unitOfWork.CommentRepo.Get(id.Value);
            if (comment == null)
                throw new ValidationException("Article not found", "");
            return new CommentDTO
            {
                ID = comment.ID,
                UserName = comment.UserName,
                Text = comment.Text,
                DateOfPublishing = comment.DateOfPublishing
            };
        }

        public IEnumerable<CommentDTO> GetComments()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Comment>, List<CommentDTO>>(unitOfWork.CommentRepo.GetAll());
        }
    }
}
