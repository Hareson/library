﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;

namespace ViewsAndControllers.BLL.Interfaces
{
    public interface IArticleService
    {
        void AddArticle(ArticleDTO articleDTO);
        ArticleDTO GetArticle(int? id);
        IEnumerable<ArticleDTO> GetArticles();
        void Dispose();
    }
}
