﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;

namespace ViewsAndControllers.BLL.Interfaces
{
    public interface IUserBookService
    {
        void AddUserBook(UserBookDTO userBookDTO);
        UserBookDTO GetUserBook(int? id);
        IEnumerable<UserBookDTO> GetUserBooks();
        void Dispose();
    }
}
