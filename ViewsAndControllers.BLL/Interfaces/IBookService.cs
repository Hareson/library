﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;

namespace ViewsAndControllers.BLL.Interfaces
{
    public interface IBookService
    {
        void AddBook(BookDTO bookDTO);
        BookDTO GetBook(int? id);
        IEnumerable<BookDTO> GetBooks();
        void Dispose();
    }
}
