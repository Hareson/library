﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.BLL.DTO;

namespace ViewsAndControllers.BLL.Interfaces
{
    public interface ICommentService
    {
        void AddComment(CommentDTO commentDTO);
        CommentDTO GetComment(int? id);
        IEnumerable<CommentDTO> GetComments();
        void Dispose();
    }
}
