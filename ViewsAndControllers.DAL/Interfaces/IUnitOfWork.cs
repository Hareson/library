﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.DAL.Entities;

namespace ViewsAndControllers.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Book> BookRepo { get; }
        IRepository<UserBook> UserBookRepo { get; }
        IRepository<Article> ArticleRepo { get; }
        IRepository<Comment> CommentRepo { get; }
        void Complete();
    }
}
