﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.DAL.EF;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.Interfaces;

namespace ViewsAndControllers.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext db;
        public Repository(DbContext context)
        {
            db = context;
        }
        public T Get(int id)
        {
            return db.Set<T>().Find(id);
        }
        public IEnumerable<T> GetAll()
        {
            return db.Set<T>().ToList();
        }
        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>().Where(predicate);
        }
        public void Add(T entity)
        {
            db.Set<T>().Add(entity);
        }
        public void AddRange(IEnumerable<T> entities)
        {
            db.Set<T>().AddRange(entities);
        }
        public void Remove(T entity)
        {
            db.Set<T>().Remove(entity);
        }
        public void RemoveRange(IEnumerable<T> entities)
        {
            db.Set<T>().RemoveRange(entities);
        }
    }
}
