﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.DAL.EF;
using ViewsAndControllers.DAL.Entities;
using ViewsAndControllers.DAL.Interfaces;
using ViewsAndControllers.DAL.Repositories;

namespace ViewsAndControllers.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext db;      

        public IRepository<UserBook> UserBookRepo { get; private set; }
        public IRepository<Book> BookRepo { get; private set; }
        public IRepository<Article> ArticleRepo { get; private set; }
        public IRepository<Comment> CommentRepo { get; private set; }

        public UnitOfWork(LibraryContext context)
        {
            db = context;
            UserBookRepo = new Repository<UserBook>(context);
            BookRepo = new Repository<Book>(context);
            ArticleRepo = new Repository<Article>(context);
            CommentRepo = new Repository<Comment>(context);
        }
        public void Complete()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
