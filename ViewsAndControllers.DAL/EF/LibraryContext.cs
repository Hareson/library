﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.DAL.Entities;

namespace ViewsAndControllers.DAL.EF
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("LibraryContextDB")
        { }
        public DbSet<Book> Books { get; set; }
        public DbSet<UserBook> UserBooks { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
