﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewsAndControllers.DAL.Entities;

namespace ViewsAndControllers.DAL.EF
{
    class LibraryInitializer : DropCreateDatabaseAlways<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            var books = new List<Book>
            {
                new Book{ Name = "Harry Potter", CheckAns = false },
                new Book{ Name = "Lord of the Ring", CheckAns = false  },
                new Book{ Name = "Game of Thrones", CheckAns = false  },
                new Book{ Name = "Witcher", CheckAns = false  },
                new Book{ Name = "IT", CheckAns = false  },
                new Book{ Name = "1984", CheckAns = false  }
            };
            books.ForEach(s => context.Books.Add(s));
            context.SaveChanges();

            var articles = new List<Article>
            {
                new Article{
                    Heading = "Article1",
                    Text = "hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Article{
                    Heading = "Article2",
                    Text = "hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Article{
                    Heading = "Article3",
                    Text = "hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Article{
                    Heading = "Article4",
                    Text = "hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Article{
                    Heading = "Article5",
                    Text = "hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                }
            };
            articles.ForEach(s => context.Articles.Add(s));
            context.SaveChanges();

            var comments = new List<Comment>
            {
                new Comment{
                    UserName = "Mykyta",
                    Text = "q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Comment{
                    UserName = "Stepan",
                    Text = "q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Comment{
                    UserName = "Myhailo",
                    Text = "q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                },
                new Comment{
                    UserName = "Nepotrib",
                    Text = "q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg hdgfdfjkhd fkjghdlfkbh viaudgf eeyrgve eruitygevb eurytgery eouryteht ertuh  qeruith3eo oq3ertouh qpth qewrtygh dfgh dfert we4rtgdf q3ertyuedrt qiuerteruh vgreui3ewq  ewuihdfgnmbyer eirughd fnkwgert weirut ghdfklg moiwertyh weuirtht kuwerbhfd yuwgerkjf tkwejgdftg",
                    DateOfPublishing = new DateTime(2021,08,23)
                }                
            };
            comments.ForEach(s => context.Comments.Add(s));
            context.SaveChanges();
        }
    }
}
